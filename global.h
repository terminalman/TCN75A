#pragma once

#define F_CPU 14745600UL
#define BAUD 9600
//-------------------------------

#define MYUBRR F_CPU/16/BAUD-1
