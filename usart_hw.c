#include <avr/io.h>

#include "usart_hw.h"

/*INIT FUNCTIONS*/
void usart_init(int ubrr){
    DDRD|=1<<PD1;
    UBRR0H = (uint8_t)(ubrr>>8);
    UBRR0L = (uint8_t)ubrr;
    UCSR0C = (1<<UCSZ01) | (1<<UCSZ00);
    UCSR0B = (1<<RXCIE0)|(1<<RXEN0)|(1<<TXEN0);
}

char usart_read_char(){
	while ( !(UCSR0A & (1<<RXC0)) );
	return UDR0;
}

void usart_send_char(char data){
    while ( !( UCSR0A & (1<<UDRE0)) );
    UDR0 = data;
}

void usart_send_string(char* data)
{
	int i=0;
	while(data[i]!='\0'){
		usart_send_char(data[i]);
		i++;
	}
}
