#include "global.h"

#include <stdlib.h>
#include <util/delay.h>
#include <avr/io.h>
#include <stdio.h>

#include "usart_hw.h"
#include "tcn75a.h"

void testled(char i)
{
	DDRB |= 1<<PB0;
	if(i){
		PORTB |= 1<<PB0;
	}
	else{
		PORTB &= ~(1<<PB0);
	}
}

int main(void)
{
	testled(1);
	usart_init(MYUBRR);

	TWBR = 100;

	int temp = 0;
	char buffer[10];

	while(1){
		tcn75a_write_reg(0x48,0x01,0x60);

		temp = tcn75a_read_temp(0x48);
		sprintf(buffer,"%d \r\n",temp);	
		usart_send_string(buffer);
		_delay_ms(500);
	}
		
	return 0;
}
