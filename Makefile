# ATMEGA328P
 
VERSION = 3.02
CC	= /usr/bin/avr-gcc
AO	= /usr/bin/avr-objcopy
OUTPUT  = flash
CFLAGS	= -mmcu=atmega328p -Wall -std=c99 -Os -o $(OUTPUT).elf
DFLAGS 	= -j .text -j .data -O ihex  
PFLAGS 	= -pm328p -cjtag2isp -Pusb -e
 
SRCS=$(wildcard *.c)
 
compile: $(OBJ)
	$(CC) $(CFLAGS) $(SRCS)
	$(AO) $(DFLAGS) $(OUTPUT).elf $(OUTPUT).hex
 
all: compile flash
 	
clean:
	rm -rf $(OUTPUT).elf $(OUTPUT).hex
 
flash:
	avrdude $(PFLAGS) -U flash:w:$(OUTPUT).hex
