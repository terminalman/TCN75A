int twi_start_condition();

void twi_end_condition();

void twi_write(unsigned int);

void twi_address_slave(char, char);

unsigned int twi_read_ack(void);

unsigned int twi_read_nack(void);
