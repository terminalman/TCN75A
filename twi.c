#include <avr/io.h>

int twi_start_condition()
{
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)));
	
	// returns -1 if start condition was not send
	if ((TWSR & 0xF8) == 0x08) return 0;
	else return -1;
}

void twi_end_condition()
{
	TWCR = (1<<TWINT)|(1<<TWEN|1<<TWSTO);
}

void twi_write(unsigned int u8data)
{
	TWDR = u8data;
	TWCR = (1<<TWINT)|(1<<TWEN);
	while ((TWCR & (1<<TWINT)) == 0);
}

void twi_address_slave(char address, char rw)
{
	if(rw=='w') rw=0;
	if(rw=='r') rw=1;

	//address the chip Write
	twi_write( (address<<1)|rw );
}

unsigned int twi_read_ack(void)
{
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
	while ((TWCR & (1<<TWINT)) == 0);
	return TWDR;
}

unsigned int twi_read_nack(void)
{
	TWCR = (1<<TWINT)|(1<<TWEN);
	while ((TWCR & (1<<TWINT)) == 0);
	return TWDR;
}
