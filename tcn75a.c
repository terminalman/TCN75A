/*
 * Copyright 2016 flutterbat
 *
 * Licensed under the GPL-2
 */

#include "twi.h"
#include "tcn75a.h"

unsigned int tcn75a_read_reg(
	unsigned char address,
	unsigned char reg)
{
	unsigned int tmp = 0;
	
	//set register reg as active
	twi_start_condition();
	twi_address_slave(address,'w');				//contact tcn75_a in write mode
	twi_write(reg);						//send register pointer value
	twi_end_condition();

	//read active register
	twi_start_condition();
	twi_address_slave(address,'r');
	
	if( reg == 0x00 || reg == 0x10 || reg == 0x11){		//read for 16bit registers
		tmp = (unsigned int)(twi_read_ack());
		tmp = tmp<<8;
		tmp |= (unsigned int)(twi_read_nack());
		twi_read_nack();
		twi_end_condition();
		return tmp;
	}
	else if(reg == 0x01){					//read for 8bit register
		tmp = (unsigned int)(twi_read_nack());
		twi_end_condition();
		return tmp;
	}
	else{
		return 0;
	}
}

unsigned int tcn75a_write_reg(
	unsigned char address,
	unsigned char reg,
	unsigned int data)
{
	//set register X as active
	twi_start_condition();
	twi_address_slave(address,'w');				//contact tcn75_a in write mode
	twi_write(reg);						//current temp register pointer
	
	//write data to active register
	if(reg == 0x10 || reg == 0x11){
		twi_write((unsigned char)(data>>8));		//write 2 bytes data
		twi_write((unsigned char)data);
		twi_end_condition();
		return 0;
	}
	else if(reg == 0x01){
		twi_write((unsigned char)(data));		//write 1 byte data
		twi_end_condition();
		return 0;
	}
	else{
		twi_end_condition();
		return 1;	
	}
}

int tcn75a_read_temp(unsigned char address)
{
	int temp = 0;
	unsigned int tmp = tcn75a_read_reg(address, 0x00);
	temp = ((tmp & 0x00f0)>>4)*100/16;
	temp += ((tmp & 0xff00)>>8)*100;
	temp |= (tmp & 0x8000);					//set pos/neg bit
	return temp;
}
