/*
 * Copyright 2016 flutterbat
 *
 * Licensed under the GPL-2
 */

/*
 * Page 15/40 Datasheet
 * Registers: 
 * 00 Ambient Temperature Register 	16bit (max used 0xFF F0)
 * 01 Sensor Configuration Register 	 8bit 
 * 10 Temerature Hysteresis Register	16bit (max used 0xFF 80)
 * 11 Temperatur Limit-Set Register	16bit (max used 0xFF F0)
 */

/*
 * Takes BUS Address, Register Address
 * Depending on the Register returns 0xXX XX or 0x00 XX 
 * Returns 0 on failure
 */
unsigned int tcn75a_read_reg(
	unsigned char, 
	unsigned char);

/*
 * Takes BUS Address, Register Address, Data 
 * Depending on the Register takes 0xXX XX or 0x00 XX
 * Returns 0 on completion
 */
unsigned int tcn75a_write_reg(
	unsigned char,
	unsigned char,
	unsigned int);

/*
 * Return fixed decimal temperature
 * 2 decimal places
 */
int tcn75a_read_temp(unsigned char);
