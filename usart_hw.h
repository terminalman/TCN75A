/*INIT FUNCTIONS*/

// initiates the usart
void usart_init( int);

// reads a uint8_tacter from the usart buffer
char usart_read_char();

// sends a single uint8_tacter
void usart_send_char( char );

// sends a string. needs '\0' terminator
void usart_send_string( char* );
